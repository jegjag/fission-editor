# Fission Editor #
## Overview ##
A tool used for asset creation for the game engine.

## Requirements ##
### End user ###
* Java 8 or later.
* Computer that is one step up from a toaster.

### For source editing ###
* JDK 8 or later.
* Eclipse Oxygen or later (Probably works on older versions but I haven't tested).
* WindowBuilder plugin if editing the UI (Although most of it is broken in WindowBuilder).

## How to use ##
Slam your head on keyboard and see what happens.