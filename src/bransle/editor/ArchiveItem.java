package bransle.editor;

import bransle.editor.Property.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import javax.swing.table.DefaultTableModel;

public abstract class ArchiveItem
{
	private ArchiveItem parent;
	
	private List<ArchiveItem> children = new ArrayList<ArchiveItem>();
	protected HashMap<String, Property> properties = new HashMap<String, Property>();
	
	public DefaultTableModel getTableModel()
	{
		DefaultTableModel model = new DefaultTableModel();
		String[] keys = properties.keySet().toArray(new String[0]);
		for(int i = 0; i < keys.length; i++)
		{
			Vector<Object> row = new Vector<Object>();
			row.add(keys[i]);
			row.add(properties.get(keys[i]).value);
			model.addRow(row);
		}
		return model;
	}
	
	public HashMap<String, Property> getProperties()
	{
		return properties;
	}
	
	public ArchiveItem(ArchiveItem parent, String name)
	{
		this.parent = parent;
		boolean edit = true;
		
		if(name.equalsIgnoreCase("root") && parent == null)		edit = false;
		
		properties.put("Name", new StringProperty(name, edit));
		if(parent == null)	this.parent = this;
	}
	
	public ArchiveItem getParent()
	{
		return parent;
	}
	
	public void add(ArchiveItem item)
	{
		children.add(item);
	}
	
	public void remove(ArchiveItem item)
	{
		children.remove(item);
	}
	
	public void addProperty(String name, Property value)
	{
		if(!properties.containsKey(name))
		{
			properties.put(name, value);
		}
	}
	
	public List<ArchiveItem> getChildren()
	{
		return children;
	}
	
	@Override
	public String toString()
	{
		return properties.get("Name").value.toString();
	}
}
