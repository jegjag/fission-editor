package bransle.editor;

import static bransle.editor.ui.lang.Lang.getString;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import bransle.editor.ui.EditorFrame;

public class Config
{
	public HashMap<String, String> entries = new HashMap<String, String>();
	
	public static final String VERSION = "1";	// Increment every time this is modified to make sure this corrects itself and doesn't crash.
	
	private static final String[] defaultEntries = new String[]{
		"game_path=res/",
		"ver=" + VERSION,
		"w=1280",
		"h=720",
		"mode=" + EditorFrame.Mode.FILE_MANAGER.name(),
		"fm_ent_frame_x=0",
		"fm_ent_frame_y=0",
		"fm_ent_frame_w=320",
		"fm_ent_frame_h=655",
		"fm_insp_frame_x=940",
		"fm_insp_frame_y=0",
		"fm_insp_frame_w=320",
		"fm_insp_frame_h=655"
	};
	
	public String getValue(String key)
	{
		return entries.get(key);
	}
	
	public int getInt(String key)
	{
		return Integer.parseInt(entries.get(key));
	}
	
	private static void create(File file)
	{
		try
		{
			file.createNewFile();
			
			FileWriter fw = new FileWriter(file);
			for(String s : defaultEntries)
			{
				fw.write(s + "\n");
			}
			fw.close();
		}catch(IOException e){
			System.err.println(getString("err_readonly"));
		}
	}
	
	public Config(File file)
	{
		if(!file.exists())
		{
			create(file);
		}
		
		try
		{
			Scanner sc = new Scanner(file);
			List<String> data = new ArrayList<String>();
			
			while(sc.hasNextLine())
			{
				data.add(sc.nextLine());
			}
			sc.close();
			String[] lines = data.toArray(new String[0]);
			
			for(String s : lines)
			{
				String[] split = s.split("=");
				if(split.length == 2)
				{
					entries.put(split[0], split[1]);
				}
				else if(split.length > 2)
				{
					String combine = split[1];
					for(int i = 2; i < split.length; i++) {
						String clone = combine;
						combine = clone + split[i];
					}
					entries.put(split[0], combine);
				}
			}
			
			if(!entries.get("ver").equalsIgnoreCase(VERSION))
			{
				System.out.println(getString("err_config_outdated"));
				file.delete();
				create(file);
				System.exit(0);
			}
		}catch(FileNotFoundException e){
			e.printStackTrace();
		}
	}
}
