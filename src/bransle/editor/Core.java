package bransle.editor;

import static bransle.editor.ui.lang.Lang.getString;

import javax.swing.UIManager;
import javax.swing.tree.DefaultTreeModel;

import bransle.archive.PakMan;
import bransle.editor.ui.EditorFrame;
import bransle.editor.ui.lang.Lang;
import bransle.editor.ui.util.ArchiveNode;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Locale;
import java.util.ArrayList;

/**
 * Both initialisation and all project data happens in here.<br>
 * <br>
 * <i>lots of stuff happening</i>
 */
public class Core
{
	private static List<ProjectListener> listeners = new ArrayList<ProjectListener>();
	
	public static Config config;
	
	public static void addListener(ProjectListener listener)
	{
		listeners.add(listener);
		listener.onInit();	// Call interface
	}
	
	public static void removeListener(ProjectListener listener)
	{
		listener.onRemove();
		listeners.remove(listener);
	}
	
	public static void callPropertyChange(ArchiveItem affectedItem)
	{
		for(ProjectListener l : listeners)
		{
			l.onEntryUpdate(affectedItem);
		}
	}
	
	public static EditorFrame frame;
	public static Project project = new Project();
	
	public static PakMan pakman;
	public static File gameDir;
	
	public static void newEntry()
	{
		project.addEntry(new Project.Entry(Project.root, getString("new_entry")));
	}
	
	public static void main(String[] args)
	{
		try
		{
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}
		catch(Exception e){}
		
		for(int i = 0; i < args.length; i++)
		{
			if(args[i].contains("locale=") || args[i].toLowerCase().contains("l="))
			{
				String localeName = args[i].split("=")[1];
				Lang.setRegion(new Locale(localeName));
			}
		}
		
		config = new Config(new File("editor.cfg"));
		gameDir = new File(config.getValue("game_path"));
		if(!gameDir.isDirectory())
		{
			System.err.println("Please enter game directory in editor.cfg");
			System.exit(0);
		}
		
		try
		{
			pakman = new PakMan(config.getValue("game_path"));
		}
		catch(FileNotFoundException e)
		{
			System.err.println("Invalid game directory '" + config.getValue("game_path") + "'.");
			System.exit(0);
		}
		
		frame = new EditorFrame();
	}
	
	public static class Project
	{
		public static final Directory root = new Directory(null, "Root");
		
		protected File saveLocation = null;
		
		public String getName()
		{
			if(saveLocation == null)
			{
				return getString("untitled");
			}
			else
			{
				if(saveLocation.getName().contains("."))
				{
					return saveLocation.getName().split(".")[0];
				}
				else return saveLocation.getName();
			}
		}
		
		public void addEntry(Entry entry)
		{
			//entries.add(entry);
			
			// Call interface
			ProjectListener[] lArr = listeners.toArray(new ProjectListener[0]);
			for(ProjectListener listener : lArr)
			{
				listener.onEntryAdded(entry);
				listener.onEntryUpdate(entry);
			}
		}
		
		public void removeEntry(Entry entry)
		{
			//entries.remove(entry);
			
			// Call interface
			ProjectListener[] lArr = listeners.toArray(new ProjectListener[0]);
			for(ProjectListener listener : lArr)
			{
				listener.onEntryRemoved(entry);
				listener.onEntryUpdate(entry);
			}
		}
		
		public static class Directory extends ArchiveItem
		{
			public Directory(ArchiveItem parent, String name)
			{
				super(parent, name);
			}
			
			public DefaultTreeModel getModel()
			{
				ArchiveNode rootNode = new ArchiveNode(this);
				
				// Add root entries
				Entry[] entries = getEntries().toArray(new Entry[0]);
				for(Entry e : entries)
				{
					rootNode.add(new ArchiveNode(e));
				}
				
				return new DefaultTreeModel(rootNode);
			}
			
			public List<Entry> getEntries()
			{
				ArchiveItem[] children = getChildren().toArray(new ArchiveItem[0]);
				List<Entry> entries = new ArrayList<Entry>();
				
				for(int i = 0; i < children.length; i++)
				{
					if(children[i] instanceof Entry)
					{
						entries.add((Entry)children[i]);
					}
				}
				return entries;
			}
			
			public List<Directory> getDirectories()
			{
				ArchiveItem[] children = getChildren().toArray(new ArchiveItem[0]);
				List<Directory> dirs = new ArrayList<Directory>();
				
				for(int i = 0; i < children.length; i++)
				{
					if(children[i] instanceof Directory)
					{
						dirs.add((Directory)children[i]);
					}
				}
				return dirs;
			}
		}
		
		public static class Entry extends ArchiveItem
		{
			public Entry(ArchiveItem parent, String name)
			{
				super(parent, name);
				
				if(parent == null)
				{
					System.err.println("Parent for Entry cannot be null.");
					return;
				}
			}
			
			@Override	@Deprecated
			public void add(ArchiveItem item)
			{
				System.err.println("Cannot add child to Entry.");
			}
			
			public String getPath()
			{
				Directory currentDir = (Directory) getParent();
				String dir = properties.get("Name").value.toString();
				
				boolean finished = false;
				while(!finished)
				{
					String dirClone = dir;
					if(currentDir.getParent() != currentDir)
					{
						dir = currentDir.getProperties().get("Name").value.toString() + "/" + dirClone;
						Directory clone = currentDir;
						currentDir = (Directory) clone.getParent();
					}
					else
					{
						dir = "/" + dirClone;
						finished = true;
					}
				}
				return dir;
			}
		}
	}
}
