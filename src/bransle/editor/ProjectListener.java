package bransle.editor;

import bransle.editor.Core.Project.Entry;

/**
 * Interface for project changes.<br>
 * <br>
 * Suggest more methods if you want at the places that are probably not in the readme at the time of writing this.
 * 
 * @author Jegjag
 */
public interface ProjectListener
{
	/**
	 * Called when Core initializes this listener.
	 */
	public void onInit();
	
	/**
	 * Called when this is removed.
	 */
	public void onRemove();
	
	/**
	 * Called when project is loaded.
	 */
	public void onLoad();
	
	/**
	 * Called when project is saved.
	 */
	public void onSave();
	
	/**
	 * Called when a change happens to an ArchiveItem
	 * (Called after <code>onEntryAdded()</code> and <code>onEntryRemoved()</code>).
	 */
	public void onEntryUpdate(ArchiveItem entry);
	
	/**
	 * Called when an entry is added to the project.
	 */
	public void onEntryAdded(Entry entry);
	
	/**
	 * Called when an entry is removed from the project.
	 */
	public void onEntryRemoved(Entry entry);
}
