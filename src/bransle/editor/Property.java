package bransle.editor;

public abstract class Property
{
	public Object value;
	protected boolean editable;
	
	public Property(Object value, boolean editable)
	{
		this.value = value;
	}
	
	public boolean isEditable()
	{
		return editable;
	}
	
	@Override
	public String toString()
	{
		return (String)value;
	}
	
	public static class StringProperty extends Property
	{
		public StringProperty(String str, boolean editable)
		{
			super(str, editable);
		}
	}
	
	public static class IntProperty extends Property
	{
		public IntProperty(Integer integer, boolean editable)
		{
			super(integer, editable);
		}
	}
	
	public static class FloatProperty extends Property
	{
		public FloatProperty(Float f, boolean editable)
		{
			super(f, editable);
		}
	}
	
	public static class DoubleProperty extends Property
	{
		public DoubleProperty(Double d, boolean editable)
		{
			super(d, editable);
		}
	}
	
	public static class LongProperty extends Property
	{
		public LongProperty(Long l, boolean editable)
		{
			super(l, editable);
		}
	}
}
