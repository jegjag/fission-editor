package bransle.editor.ui;

import static bransle.editor.ui.lang.Lang.getString;
import static bransle.editor.Core.config;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JMenu;
import javax.swing.JRadioButtonMenuItem;

import bransle.editor.ArchiveItem;
import bransle.editor.Core;
import bransle.editor.Core.Project.Entry;
import bransle.editor.ProjectListener;
import bransle.editor.ui.panel.*;
import bransle.editor.ui.renderer.StyledTreeCellRenderer;

import javax.swing.JMenuItem;

/*
 * WindowBuilder doesn't like editing this because size is from config which is handled at init.
 */
public class EditorFrame extends JFrame
{
	private static final long serialVersionUID = -6515486383563978303L;
	
	// Listener stuff
	private List<UIListener> uiListeners = new ArrayList<UIListener>();
	
	// Colours to use
	public static final Color	UI_TEXT_COLOR	= new Color(255, 255, 255);
	public static final Font	UI_TEXT_FONT	= new Font("Open Sans", Font.PLAIN, 12);
	public static final Color	UI_TITLE_COLOR	= new Color(255, 255, 255);
	public static final Font	UI_TITLE_FONT	= new Font("Open Sans", Font.BOLD, 16);
	
	public static final Color	UI_BACKGROUND = new Color(45, 45, 45);
	public static final Color	UI_BORDER = new Color(20, 20, 20);
	public static final Color	UI_WINDOW_BACKGROUND = new Color(40, 40, 40);
	public static final Color	UI_SELECTED_BACKGROUND = new Color(80, 80, 80);
	
	// Renderers
	public static final StyledTreeCellRenderer RENDERER_TREE = new StyledTreeCellRenderer(UI_TEXT_COLOR, UI_WINDOW_BACKGROUND, UI_SELECTED_BACKGROUND);
	
	// Get UI sizes to remove
	public static int getRemoveX()
	{
		return 11;
	}
	
	public static int getRemoveY()
	{
		return 10;
	}
	
	public void addListener(UIListener listener)
	{
		uiListeners.add(listener);
		listener.onInit();
	}
	
	public void removeListener(UIListener listener)
	{
		listener.onRemove();
		uiListeners.remove(listener);
	}
	
	private ProjectListener projectListener;
	
	// Editor Mode stuff
	private Mode mode = Mode.FILE_MANAGER;
	
	private void setMode(Mode mode)
	{
		this.mode = mode;
		
		UIListener[] listeners = uiListeners.toArray(new UIListener[0]);
		for(UIListener listener : listeners)
		{
			listener.onModeChange(mode);
		}
	}
	
	public Mode getMode()
	{
		return mode;
	}
	
	/**
	 * Create the panel.
	 */
	public EditorFrame()
	{
		setTitle("BransleEdit - " + Core.project.getName());
		//setSize(config.getInt("w"), config.getInt("h"));
		setSize(1280, 720);	// TEMP (TODO)
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		
		// Set up projectListener
		projectListener = new ProjectListener()
		{
			@Override
			public void onInit()
			{
				setTitle("BransleEdit - " + Core.project.getName() + " (" + mode.name + ")");
			}
			
			@Override
			public void onRemove(){}
			
			@Override
			public void onLoad()
			{
				setTitle("BransleEdit - " + Core.project.getName() + " (" + mode.name + ")");
			}
			
			@Override
			public void onSave()
			{
				setTitle("BransleEdit - " + Core.project.getName() + " (" + mode.name + ")");
			}
			
			@Override
			public void onEntryUpdate(ArchiveItem entry){};
			
			@Override
			public void onEntryAdded(Entry entry){}
			
			@Override
			public void onEntryRemoved(Entry entry){}
		};
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu menu_file = new JMenu(getString("menu_file"));
		menuBar.add(menu_file);
		
		JMenuItem mntmSave = new JMenuItem(getString("menu_file_save"));
		menu_file.add(mntmSave);
		
		JMenu menu_view = new JMenu(getString("menu_view"));
		menuBar.add(menu_view);
		
		JMenu menu_view_mode = new JMenu(getString("menu_view_mode"));
		menu_view.add(menu_view_mode);
		
		ButtonGroup modeGroup = new ButtonGroup();
		
		// Add all modes to dropdown
		final Mode[] modes = Mode.values();
		for(final Mode m : modes)
		{
			JRadioButtonMenuItem item = new JRadioButtonMenuItem(m.name);
			item.addActionListener(new ActionListener()
			{
				@Override
				public void actionPerformed(ActionEvent e)
				{
					setMode(m);
				}
			});
			
			if(modes.length > 0 && modes[0].equals(m))
				item.setSelected(true);
			
			modeGroup.add(item);
			menu_view_mode.add(item);
		}
		
		getContentPane().setLayout(new CardLayout(0, 0));
		
		for(Mode m : modes)
		{
			if(config.entries.get("mode").equalsIgnoreCase(m.name))
			{
				mode = m;
			}
			getContentPane().add(m.panel, m.name);
		}
		
		CardLayout cl = (CardLayout) getContentPane().getLayout();
		cl.show(getContentPane(), mode.name);
		
		Core.addListener(projectListener);
		addListener(new UIListener()
		{
			@Override
			public void onInit(){}
			
			@Override
			public void onRemove(){}
			
			@Override
			public void onModeChange(Mode mode)
			{
				setTitle("BransleEdit - " + Core.project.getName() + " (" + mode.name + ")");
				CardLayout cl = (CardLayout) getContentPane().getLayout();
				cl.show(getContentPane(), mode.name);
			}
		});
		
		setVisible(true);
	}
	
	public static enum Mode
	{
		FILE_MANAGER("mode_fm", new FMPanel()),
		MAP_EDITOR("mode_map", new MapPanel()),
		OBJECT_EDITOR("mode_object", new ObjectEditorPanel());
		
		public final String name;
		public final JPanel panel;
		
		Mode(String nameLookup, JPanel editorPanel)
		{
			name = getString(nameLookup);
			panel = editorPanel;
		}
	}
}
