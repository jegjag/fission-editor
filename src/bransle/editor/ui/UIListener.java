package bransle.editor.ui;

/**
 * Interface for UI Updates (Changing mode, etc.)
 */
public interface UIListener
{
	/**
	 * Called when this is loaded.
	 */
	public void onInit();
	
	/**
	 * Called when this is removed.
	 */
	public void onRemove();
	
	/**
	 * Called when mode is changed.
	 */
	public void onModeChange(EditorFrame.Mode mode);
}
