package bransle.editor.ui.listeners;

import static bransle.editor.ui.EditorFrame.getRemoveX;
import static bransle.editor.ui.EditorFrame.getRemoveY;

import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JInternalFrame;

import bransle.editor.Core;

public class InternalPanelBasicListener implements MouseMotionListener, MouseListener
{
	private Point point = null, current = null;
	private JInternalFrame internalFrame;
	
	public InternalPanelBasicListener(JInternalFrame internalFrame)
	{
		this.internalFrame = internalFrame;
		this.internalFrame.setVisible(false);
	}
	
	@Override
	public void mouseDragged(MouseEvent e)
	{
		current = e.getLocationOnScreen();
		
		int x = current.x - getRemoveX() - Core.frame.getX() - Core.frame.getContentPane().getX() - point.x;
		int y = current.y - Core.frame.getY() - Core.frame.getContentPane().getY()*2 - point.y - getRemoveY();
		
		if(x < 0)	x = 0;
		if(y < 0)	y = 0;
		if(x > Core.frame.getSize().width - internalFrame.getWidth())		x = Core.frame.getSize().width - internalFrame.getWidth();
		if(y > Core.frame.getSize().height - internalFrame.getHeight())		y = Core.frame.getSize().height - internalFrame.getHeight();
		
		internalFrame.setLocation(x, y);
	}
	
	@Override
	public void mouseMoved(MouseEvent e){}

	@Override
	public void mouseClicked(MouseEvent e){}

	@Override
	public void mouseEntered(MouseEvent e){}

	@Override
	public void mouseExited(MouseEvent e){}

	@Override
	public void mousePressed(MouseEvent e)
	{
		point = e.getPoint();
	}

	@Override
	public void mouseReleased(MouseEvent e)
	{
		point = null;
	}
}
