package bransle.editor.ui.panel;

import static bransle.editor.ui.lang.Lang.getString;
import static bransle.editor.ui.EditorFrame.*;

import javax.swing.JPanel;
import javax.swing.JPopupMenu;

import java.awt.BorderLayout;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JMenuItem;
import javax.swing.JTree;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.plaf.basic.BasicInternalFrameUI;
import javax.swing.table.DefaultTableModel;
import javax.swing.JTable;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import bransle.editor.ArchiveItem;
import bransle.editor.Core;
import bransle.editor.Core.Project;
import bransle.editor.Core.Project.Entry;
import bransle.editor.ProjectListener;
import bransle.editor.Property;
import bransle.editor.ui.listeners.InternalPanelBasicListener;
import bransle.editor.ui.util.ArchiveNode;
import javax.swing.border.MatteBorder;
import java.awt.Dimension;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import java.awt.FlowLayout;

public class FMPanel extends JPanel
{
	private static final long serialVersionUID = -2798877368106134510L;
	public JTree entriesTree;
	
	private List<FMListener> listeners = new ArrayList<FMListener>();
	public JTable table;
	
	private ArchiveItem selectedEntry = null;
	
	private InternalPanelBasicListener entriesPaneListener, inspPaneListener;
	
	/**
	 * Create the panel.
	 */
	public FMPanel()
	{
		setLayout(new BorderLayout(0, 0));
		
		JDesktopPane desktopPane = new JDesktopPane();
		desktopPane.setBackground(UI_BACKGROUND);
		add(desktopPane, BorderLayout.CENTER);
		
		final JInternalFrame entriesPane = new JInternalFrame(getString("title_entries"));
		entriesPane.setBorder(new MatteBorder(3, 3, 3, 3, UI_BORDER));
		entriesPane.setResizable(true);
		entriesPane.setBounds(0, 0, 320, 655);	// TODO Get from config
		entriesPane.setMinimumSize(new Dimension(200, 150));
		
		// Hide title
		BasicInternalFrameUI entriesPaneUI = (BasicInternalFrameUI) entriesPane.getUI();
		entriesPaneUI.setNorthPane(null);
		
		desktopPane.add(entriesPane);
		
		entriesTree = new JTree();
		entriesTree.setBackground(UI_WINDOW_BACKGROUND);
		entriesTree.setCellRenderer(RENDERER_TREE);
		entriesTree.setModel(Project.root.getModel());
		entriesTree.addTreeSelectionListener(new TreeSelectionListener()
		{
			@Override
			public void valueChanged(TreeSelectionEvent e)
			{
				ArchiveNode node = (ArchiveNode) entriesTree.getLastSelectedPathComponent();
				if(node == null)	return;
				
				// On select
				FMListener[] listenerArr = listeners.toArray(new FMListener[0]);
				for(FMListener l : listenerArr)
				{
					l.onTableSelectionChange(node);
				}
			}
		});
		
		JPopupMenu treeRC = new JPopupMenu();
		JMenuItem createEntry = new JMenuItem(getString("create_entry"));
		createEntry.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				Core.newEntry();
			}
		});
		treeRC.add(createEntry);
		
		JPanel titlebar = new JPanel();
		titlebar.setBackground(UI_BORDER);
		titlebar.setLayout(new BorderLayout(0, 0));
		
		// Make it work like a titlebar
		entriesPaneListener = new InternalPanelBasicListener(entriesPane);
		titlebar.addMouseListener(entriesPaneListener);
		titlebar.addMouseMotionListener(entriesPaneListener);
		
		entriesPane.getContentPane().add(titlebar, BorderLayout.NORTH);
		
		JPanel iconPanel = new JPanel();
		iconPanel.setBackground(UI_BORDER);
		titlebar.add(iconPanel, BorderLayout.WEST);
		
		JLabel icon = new JLabel("");
		icon.setIcon(null);	// TODO Put icon here eventually
		iconPanel.add(icon);
		
		JLabel title = new JLabel(getString("title_entries"));
		title.setForeground(UI_TITLE_COLOR);
		title.setBackground(UI_BORDER);
		title.setFont(new Font("Open Sans", Font.BOLD, 16));
		titlebar.add(title, BorderLayout.CENTER);
		
		entriesPane.getContentPane().add(entriesTree, BorderLayout.CENTER);
		
		// Add project listener for JTree
		Core.addListener(new ProjectListener()
		{
			@Override
			public void onInit(){}
			
			@Override
			public void onRemove(){}
			
			@Override
			public void onLoad()
			{
				updateTree();
			}
			
			@Override
			public void onSave(){}
			
			@Override
			public void onEntryUpdate(ArchiveItem item)
			{
				updateTree();
			}
			
			@Override
			public void onEntryAdded(Entry entry){}
			
			@Override
			public void onEntryRemoved(Entry entry){}
		});
		
		JInternalFrame inspectorPane = new JInternalFrame(getString("title_inspector"));
		inspectorPane.setResizable(true);
		inspectorPane.setBounds(939, 0, 320, 655);	// TODO Get from config
		inspectorPane.setMinimumSize(new Dimension(200, 150));
		inspectorPane.getContentPane().setLayout(new BorderLayout(0, 0));
		inspectorPane.setBackground(UI_WINDOW_BACKGROUND);
		inspectorPane.setBorder(new MatteBorder(3, 3, 3, 3, UI_BORDER));
		
		// Hide title
		BasicInternalFrameUI inspPaneUI = (BasicInternalFrameUI) inspectorPane.getUI();
		inspPaneUI.setNorthPane(null);
		
		JPanel inspTitleBar = new JPanel();
		
		inspPaneListener = new InternalPanelBasicListener(inspectorPane);
		inspTitleBar.addMouseListener(inspPaneListener);
		inspTitleBar.addMouseMotionListener(inspPaneListener);
		
		desktopPane.add(inspectorPane);
		
		inspTitleBar.setBackground(new Color(20, 20, 20));
		inspectorPane.getContentPane().add(inspTitleBar, BorderLayout.NORTH);
		inspTitleBar.setLayout(new BorderLayout(0, 0));
		
		JPanel inspIconPanel = new JPanel();
		inspIconPanel.setBackground(UI_BORDER);
		inspTitleBar.add(inspIconPanel, BorderLayout.WEST);
		inspIconPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JLabel inspIcon = new JLabel("");
		inspIcon.setIcon(null);
		inspIconPanel.add(inspIcon);
		
		JLabel label = new JLabel(getString("title_inspector"));
		label.setForeground(UI_TITLE_COLOR);
		label.setFont(UI_TITLE_FONT);
		label.setBackground(UI_BORDER);
		inspTitleBar.add(label, BorderLayout.CENTER);
		
		table = new JTable()
		{
			private static final long serialVersionUID = 165846790798765654L;
			
			// Set certain cells as uneditable
			@Override
			public boolean isCellEditable(int row, int col)
			{
				if(col == 0)	return false;
				else
				{
					DefaultTableModel model = (DefaultTableModel) table.getModel();
					return selectedEntry.getProperties().get(model.getValueAt(row, 0)).isEditable();
				}
			}
		};
		inspectorPane.getContentPane().add(table, BorderLayout.CENTER);
		table.setFillsViewportHeight(true);
		table.setModel(new DefaultTableModel(
			new Object[][]{},
			new String[]{"Key", "Value"}
		));
		table.getModel().addTableModelListener(new TableModelListener()
		{
			@Override
			public void tableChanged(TableModelEvent e)
			{
				switch(e.getType())
				{
					case TableModelEvent.UPDATE:	// On update call update listeners
						for(FMListener l : listeners)
						{
							l.onCellChange(e.getColumn(), e.getFirstRow());
						}
						break;
				}
			}
		});
		
		table.setBackground(UI_WINDOW_BACKGROUND);
		table.setSelectionBackground(UI_SELECTED_BACKGROUND);
		table.setForeground(UI_TEXT_COLOR);
		table.setFont(new Font("Open Sans", Font.PLAIN, 12));
		
		inspectorPane.setVisible(true);
		entriesPane.setVisible(true);
		
		// Add listener to update inspector when JTree updates
		addListener(new FMListener()
		{
			@Override
			public void onTableSelectionChange(ArchiveNode node)
			{
				// Update inspector
				selectedEntry = node.entry;
				DefaultTableModel model = (DefaultTableModel) table.getModel();
				
				// Clear
				for(int i = 0; i < model.getRowCount(); i++)
				{
					model.removeRow(i);
				}
				
				// Get properties
				String[] propertyKeys = selectedEntry.getProperties().keySet().toArray(new String[0]);
				List<Property> properties_list = new ArrayList<Property>();
				for(String key : propertyKeys)
				{
					properties_list.add(selectedEntry.getProperties().get(key));
				}
				Property[] properties = properties_list.toArray(new Property[0]);
				for(int i = 0; i < properties.length; i++)
				{
					model.addRow(new Object[]
					{
							propertyKeys[i],
							properties[i].value
					});
				}
			}
			
			@Override
			public void onCellChange(int col, int row)
			{
				Core.callPropertyChange(selectedEntry);
			}
		});
	}
	
	public void updateTree()
	{
		entriesTree.setModel(Project.root.getModel());
	}
	
	public void addListener(FMListener listener)
	{
		listeners.add(listener);
	}
	
	public static interface FMListener
	{
		/**
		 * Called when the selected entry in the JTree is changed.
		 */
		public void onTableSelectionChange(ArchiveNode node);
		
		/**
		 * Called when a cell has been changed in the inspector.
		 */
		public void onCellChange(int col, int row);
	}
}
