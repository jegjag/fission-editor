package bransle.editor.ui.panel;

import static bransle.editor.ui.lang.Lang.getString;

import javax.swing.JPanel;
import javax.swing.JDesktopPane;
import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.JInternalFrame;

public class MapPanel extends JPanel
{
	private static final long serialVersionUID = -3017529915275395911L;
	
	private JInternalFrame rendererOutput;
	
	/**
	 * Create the panel.
	 */
	public MapPanel()
	{
		setLayout(new BorderLayout(0, 0));
		
		JDesktopPane basePanel = new JDesktopPane();
		basePanel.setBackground(Color.LIGHT_GRAY);
		add(basePanel);
		
		rendererOutput = new JInternalFrame(getString("render_output"));
		rendererOutput.setResizable(true);
		rendererOutput.setMaximizable(true);
		rendererOutput.setBounds(10, 11, 800, 450);
		basePanel.add(rendererOutput);
		rendererOutput.setVisible(true);
	}
}
