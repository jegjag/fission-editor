package bransle.editor.ui.renderer;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;

public class StyledTreeCellRenderer extends DefaultTreeCellRenderer
{
	private static final long serialVersionUID = -5222059250058719811L;
	
	public Color textColor, bgColor, selectedColor;
	
	public StyledTreeCellRenderer(Color textColor, Color bgColor, Color selectedColor)
	{
		this.textColor = textColor;
		this.bgColor = bgColor;
		this.selectedColor = selectedColor;
	}
	
	@Override
	public Component getTreeCellRendererComponent(JTree tree, Object value,
			boolean sel, boolean exp, boolean leaf, int row, boolean hasFocus)
	{
		super.getTreeCellRendererComponent(tree, value, sel, exp, leaf, row, hasFocus);
		
		if(leaf)
		{
			setForeground(textColor);
			setBackgroundNonSelectionColor(bgColor);
			setBackgroundSelectionColor(selectedColor);
		}
		
		return this;
	}
}
