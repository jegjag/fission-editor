package bransle.editor.ui.util;

import javax.swing.tree.DefaultMutableTreeNode;

import bransle.editor.ArchiveItem;

public class ArchiveNode extends DefaultMutableTreeNode{
	private static final long serialVersionUID = -3677009595224840541L;
	
	public ArchiveItem entry;
	
	public ArchiveNode(ArchiveItem entry)
	{
		super(entry.getProperties().get("Name"));
		this.entry = entry;
	}
}
